#include "customtreedelegate.h"
#include <QPainter>
#include <QDebug>
#include <QApplication>
#include <QPushButton>
#include "editorwidget.h"
#include "treeitem.h"
#include "treemodel.h"

CustomTreeDelegate::CustomTreeDelegate() : QStyledItemDelegate(nullptr)
{
    installEventFilter(this);

}

void CustomTreeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    TreeItem *item = reinterpret_cast<TreeItem*>(index.internalPointer());
    QString iconPath;
    QRect r;
    QImage icon;


    QStyleOptionButton opt;
    QFontMetrics metric(painter->font());

    switch (item->getItemData().type) {
    case 0:
        painter->save();
        iconPath = ":/res/Countries/" + item->getItemData().countryCode + ".png";

        icon.load(iconPath);
        painter->drawImage(QPointF(option.rect.x(), option.rect.y()), icon);
        r = QRect(option.rect.x() + icon.width() ,option.rect.y(),option.rect.width() - icon.width(),option.rect.height());
        //r.setLeft(20);
        painter->drawText(r, item->getItemData().countryName);
        painter->restore();

        break;

    case 1:
        painter->save();


        iconPath = ":/res/Operators/" + QString::number(item->getItemData().mcc) + "_" + QString::number(item->getItemData().mnc) + ".png";

        icon.load(iconPath);
        painter->drawImage(QPointF(option.rect.x(), option.rect.y()), icon);
        r = QRect(option.rect.x() + icon.width() ,option.rect.y(),option.rect.width() - icon.width(),option.rect.height());


        painter->drawText(r, index.data().toString());

        if (option.state & QStyle::State_MouseOver)
        {
            opt.iconSize = QSize(option.rect.height(), option.rect.height());
            opt.icon = QIcon(":/res/controls/plus.png");
            opt.rect = QRect(r.x()+metric.horizontalAdvance(index.data().toString()), option.rect.y(),
                             opt.iconSize.width(), opt.iconSize.height());
            //        opt.se
            if( QWidget* w = dynamic_cast< QWidget* >( painter->device() ) )
            {
                if( w->isEnabled() )
                    opt.state |= QStyle::State_Enabled;
                if( w->window()->isActiveWindow() )
                    opt.state |= QStyle::State_Active;
            }	else
            {
                opt.state |= QStyle::State_Enabled;
            }
            QApplication::style()->drawControl( QStyle::CE_PushButton, &opt, painter );
        }
        painter->restore();

        break;

    default:

        break;
    }
}

QWidget *CustomTreeDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    TreeItem* item = reinterpret_cast<TreeItem*>(index.internalPointer());

    if(item->getItemData().type == TreeItem::OPERATOR){
        return new EditorWidget(index);
    }else if(item->getItemData().type == TreeItem::COUNTRY){
        return nullptr;
    }
    return QStyledItemDelegate::createEditor(parent, option, index);
}

void CustomTreeDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(index)
    EditorWidget* editorWidget = static_cast<EditorWidget*>(editor);
    editorWidget->updateWidget();
}

void CustomTreeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    EditorWidget* edit = static_cast<EditorWidget*>(editor);
    if(edit->getSaveData()){
        model->setData(index, edit->getOperatorName());
    }
}
