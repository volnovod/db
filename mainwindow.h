#pragma once

#include <QMainWindow>
#include "country.h"

class DataBaseAccessor;
class QSqlTableModel;
class TreeModel;
class QPushButton;
class CustomTreeDelegate;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void setOperatosModel(QList<Country> data);

    void needDataUpdate(QString name, QString mcc, QString mnc);


private:

    void setModel();

    Ui::MainWindow      *ui;
    DataBaseAccessor*    m_dataBaseAccessor = nullptr;
    TreeModel*           m_model = nullptr;
    QPushButton*         m_addButton;
    QThread*             m_thread = nullptr;

private slots:
    void on_addbutton_clicked();
};
