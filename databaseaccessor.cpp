#include "databaseaccessor.h"
#include "treeitem.h"

DataBaseAccessor::DataBaseAccessor(QObject *parent) : QObject(parent)
{

}

DataBaseAccessor::~DataBaseAccessor()
{
    if(dataBase.isOpen()){
        dataBase.removeDatabase("QSQLITE");
        dataBase.close();
    }

    qDebug() << "DataBaseAccessor DESTRUCTOR";
}

bool DataBaseAccessor::createConnection()
{
    dataBase = QSqlDatabase::addDatabase("QSQLITE");
    QString dbName = QDir::currentPath() + "/system.db";
    dataBase.setDatabaseName(dbName);
    if(!dataBase.open()){
        qDebug() << "DataBase open error, " << dataBase.lastError();
        return false;
    }

    prepareData();

    return true;
}

void DataBaseAccessor::prepareData()
{
    data = getCountries();

    emit setOperatorsModel(data);
}

void DataBaseAccessor::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    Q_UNUSED(roles)
    Q_UNUSED(bottomRight)
    TreeItem* item = static_cast<TreeItem*>(topLeft.internalPointer());
    if(item->getItemData().type == TreeItem::OPERATOR){
        int mcc = item->getItemData().mcc;
        int mnc = item->getItemData().mnc;
        QString name = item->getItemData().operatorName;
            QString qr = "UPDATE operators SET name='" + name + "' WHERE mcc=" + QString::number(mcc) + " AND mnc=" + QString::number(mnc) + ";";
            QSqlQuery query;

            if(!query.exec(qr)){
                qDebug() << "Error";
            }
    }
}

void DataBaseAccessor::createOperator(const QString &name, const QString &mcc, const QString &mnc)
{
    QString qr = "INSERT INTO operators (name, mcc, mnc) VALUES ('" + name + "', " + mcc + ", " + mnc + ");";
    QSqlQuery query;

    if(!query.exec(qr)){
        qDebug() << "Error";
        return;
    }
    emit needDataUpdate(name, mcc, mnc);
}

QList<Operator> DataBaseAccessor::getOperatorsByMcc(int mcc)
{
    QList<Operator> operators;
    if(mcc < 200 || (mcc > 800 && mcc <899) || mcc == 901)
        return operators;

    QString qr = "SELECT * FROM operators WHERE mcc=" + QString::number(mcc) + ";";
    QSqlQuery query;

    if(!query.exec(qr)){
        qDebug() << "Error";
    }


    QSqlRecord rec = query.record();



    while (query.next()) {
        Operator curOperator;
        curOperator.setMcc(query.value(rec.indexOf("mcc")).toInt());
        curOperator.setMnc(query.value(rec.indexOf("mnc")).toInt());
        curOperator.setName(query.value(rec.indexOf("name")).toString());
        if(curOperator.getMcc() == mcc){
            operators.append(curOperator);
        }
    }
    return operators;
}

QList<Country> DataBaseAccessor::getCountries()
{
    QList<Country> countries;
    QString qr = "SELECT * FROM countries";
    QSqlQuery query;

    if(!query.exec(qr)){
        qDebug() << "Error";
    }


    QSqlRecord rec = query.record();


    while (query.next()) {
        Country curCountry = Country();
        curCountry.setMcc(query.value(rec.indexOf("mcc")).toInt());
        if(curCountry.getMcc() < 200 || (curCountry.getMcc() > 800 && curCountry.getMcc() <899) || curCountry.getMcc() == 901){

        }else{
            curCountry.setCode(query.value(rec.indexOf("code")).toString());
            curCountry.setName(query.value(rec.indexOf("name")).toString());
            curCountry.setMnc_length(query.value(rec.indexOf("mnc_length")).toInt());
            curCountry.setOperatorsList(getOperatorsByMcc(curCountry.getMcc()));
            countries.append(curCountry);
        }
    }
    return countries;
}

QList<Country> DataBaseAccessor::getData() const
{
    return data;
}
