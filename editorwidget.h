#pragma once

#include <QWidget>

class QModelIndex;
class TreeItem;
class TreeModel;

namespace Ui {
class EditorWidget;
}

class EditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditorWidget(const QModelIndex &index, QWidget *parent = nullptr);
    EditorWidget(const TreeModel *model, QWidget *parent = nullptr);
    ~EditorWidget();

    void updateWidget();

    bool getSaveData() const;

    QVariant getOperatorName();

    bool getEdit() const;

private slots:
    void on_saveButton_clicked();

    void on_cancelButton_clicked();

    void on_mcc_textChanged(const QString &arg1);

    void on_mnc_textChanged(const QString &arg1);

    void on_operatorName_textChanged(const QString &arg1);

signals:
    void createOperator(QString name, QString mcc, QString mnc);

private:

    void updateCountryIcon(QString countryCode);
    void updateOperatorIcon(QString name);
    void updateStatusSave();

    Ui::EditorWidget *ui;
    TreeItem* item;
    const TreeModel* m_model;
    bool saveData = false;
    bool edit = true;
};

