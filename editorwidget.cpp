#include "editorwidget.h"
#include "ui_editorwidget.h"
#include <QModelIndex>
#include <QDebug>
#include "treeitem.h"
#include "treemodel.h"

EditorWidget::EditorWidget(const QModelIndex &index, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditorWidget)
{
    ui->setupUi(this);
    item = reinterpret_cast<TreeItem*>(index.internalPointer());
    edit = true;
}

EditorWidget::EditorWidget(const TreeModel* model, QWidget *parent):
    QWidget(parent),
    ui(new Ui::EditorWidget)
{
    setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    ui->setupUi(this);
    edit = false;
    m_model = model;
}

EditorWidget::~EditorWidget()
{
    delete ui;
}

void EditorWidget::updateWidget()
{
    QList<QWidget*> list = findChildren<QWidget*>();

    foreach (QWidget* w, list) {
        w->blockSignals(true);
    }

    ui->mcc->setText(QString::number(item->getItemData().mcc));
    ui->mnc->setText(QString::number(item->getItemData().mnc));
    ui->operatorName->setText(item->getItemData().operatorName);
    updateOperatorIcon(ui->mcc->text() + "_" + ui->mnc->text());
    updateCountryIcon(item->parent()->getItemData().countryCode);
    ui->saveButton->setEnabled(true);
    ui->mcc->setEnabled(false);
    ui->mnc->setEnabled(false);

    foreach (QWidget* w, list) {
        w->blockSignals(false);
    }
}

void EditorWidget::on_saveButton_clicked()
{
    if(!edit){
        if(!m_model->getCountryCodeByMcc(ui->mcc->text().toInt()).isEmpty()){
            emit createOperator(ui->operatorName->text(), ui->mcc->text(), ui->mnc->text());
        }
    }
    saveData = true;
    close();
}

void EditorWidget::on_cancelButton_clicked()
{
    saveData = false;
    close();
}

bool EditorWidget::getEdit() const
{
    return edit;
}

bool EditorWidget::getSaveData() const
{
    return saveData;
}

QVariant EditorWidget::getOperatorName()
{
    return ui->operatorName->text();
}

void EditorWidget::on_mcc_textChanged(const QString &arg1)
{
    if(arg1.size() == 3){
        updateCountryIcon(m_model->getCountryCodeByMcc(arg1.toInt()));
    }
    updateOperatorIcon(arg1 + "_" + ui->mnc->text());
    updateStatusSave();
}

void EditorWidget::updateCountryIcon(QString countryCode)
{
    QString iconName = ":/res/Countries/" + countryCode + ".png";
    ui->countryIcon->setPixmap(QPixmap(iconName));
}

void EditorWidget::updateOperatorIcon(QString name)
{
    QString iconName = ":/res/Operators/" + name + ".png";
    ui->operatorIcon->setPixmap(QPixmap(iconName));
}

void EditorWidget::updateStatusSave()
{
    if(!ui->operatorName->text().isEmpty() && !ui->mcc->text().isEmpty() && !ui->mnc->text().isEmpty()){
        ui->saveButton->setEnabled(true);
        return;
    }
    ui->saveButton->setEnabled(false);
}

void EditorWidget::on_mnc_textChanged(const QString &arg1)
{
    updateOperatorIcon(ui->mcc->text() + "_" + arg1);
    updateStatusSave();
}

void EditorWidget::on_operatorName_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1)
    updateStatusSave();
}
