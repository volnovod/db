#include "treemodel.h"
#include "treeitem.h"

#include <QtWidgets>

TreeModel::TreeModel(const QList<Country> &data, QObject *parent)
    : QAbstractItemModel(parent) {
    TreeItem::Data rootData;
    rootItem = new TreeItem(rootData);
    setupModelData(data, rootItem);
}

TreeModel::~TreeModel(){
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    TreeItem *item = getItem(index);

    return item->data();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

TreeItem *TreeModel::getItem(const QModelIndex &index) const {
    if (index.isValid()) {
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const {
    Q_UNUSED(section)
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data();

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeItem *parentItem = getItem(parent);
    if (!parentItem)
        return QModelIndex();

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    return QModelIndex();
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex &parent)
{
    beginInsertColumns(parent, position, position + columns - 1);
    //    const bool success = rootItem->insertColumns(position, columns);
    endInsertColumns();

    return /*success*/true;
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginInsertRows(parent, position, position + rows - 1);
    const bool success = parentItem->insertChildren(position,
                                                    rows,
                                                    rootItem->columnCount());
    endInsertRows();

    return success;
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = getItem(index);
    TreeItem *parentItem = childItem ? childItem->parent() : nullptr;

    if (parentItem == rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

bool TreeModel::removeColumns(int position, int columns, const QModelIndex &parent)
{
    beginRemoveColumns(parent, position, position + columns - 1);
    //    const bool success = rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return /*success*/true;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    const bool success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

QString TreeModel::getCountryCodeByMcc(int mcc) const
{
    for(int i=0; i < rootItem->childCount(); i++){
        if(rootItem->child(i)->getItemData().mcc == mcc && rootItem->child(i)->getItemData().type == TreeItem::COUNTRY){
            return rootItem->child(i)->getItemData().countryCode;
        }
    }
    return QString();
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    const TreeItem *parentItem = getItem(parent);

    return parentItem ? parentItem->childCount() : 0;
}

bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

        TreeItem *item = getItem(index);
        bool result = false;
        if(item->getItemData().type == TreeItem::OPERATOR){
            item->setOperatorName(value);
            result = true;
        }

    if (result)
        emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

    return result;
}

void TreeModel::addOperatorToModel(QString name, QString mcc, QString mnc)
{
    for(int i=0; i < rootItem->childCount(); i++){
        TreeItem* parent = rootItem->child(i);
        if(rootItem->child(i)->getItemData().mcc == mcc.toInt() && rootItem->child(i)->getItemData().type == TreeItem::COUNTRY
                && parent->parent() == rootItem){
            TreeItem::Data itemData;
            itemData.type = TreeItem::OPERATOR;
            itemData.operatorName = name;
            itemData.mcc = mcc.toInt();
            itemData.mnc = mnc.toInt();

            parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());

            parent->child(parent->childCount() - 1)->setData(itemData);
            QModelIndex index;
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
        }
    }
}

void TreeModel::setupModelData(const QList<Country> &data, TreeItem *parent)
{
    QVector<TreeItem*> parents;
    parents << parent;

    for(Country curCountry : data) {

        TreeItem::Data itemData;
        itemData.type = TreeItem::COUNTRY;
        itemData.countryCode = curCountry.getCode();
        itemData.countryName = curCountry.getName();
        itemData.mcc = curCountry.getMcc();
        itemData.mncLength = curCountry.getMnc_length();

        TreeItem *parent = parents.last();
        parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());

        parent->child(parent->childCount() - 1)->setData(itemData);

        parent = parent->child(parent->childCount() - 1);
        for(Operator curOperator : curCountry.getOperatorsList()){
            TreeItem::Data itemData;
            itemData.type = TreeItem::OPERATOR;
            itemData.operatorName = curOperator.getName();
            itemData.mcc = curOperator.getMcc();
            itemData.mnc = curOperator.getMnc();

            parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());

            parent->child(parent->childCount() - 1)->setData(itemData);
        }
    }
}
