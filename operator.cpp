#include "operator.h"

Operator::Operator(int mcc, int mnc, QString name)
{
    this->mcc = mcc;
    this->mnc = mnc;
    this->name = name;
}

int Operator::getMcc() const
{
    return mcc;
}

void Operator::setMcc(int value)
{
    mcc = value;
}

int Operator::getMnc() const
{
    return mnc;
}

void Operator::setMnc(int value)
{
    mnc = value;
}

QString Operator::getName() const
{
    return name;
}

void Operator::setName(const QString &value)
{
    name = value;
}
