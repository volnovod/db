#include "country.h"

Country::Country(int mcc, int mnc_length, QString code, QString name, QList<Operator> operatorsList)
{
//    qRegisterMetaType<Country>("Country");
    this->mcc = mcc;
    this->mnc_length = mnc_length;
    this->code = code;
    this->name = name;
    this->operatorsList = operatorsList;

}

Country::Country(const Country &copy)
{
    this->mcc = copy.mcc;
    this->mnc_length = copy.mnc_length;
    this->code = copy.code;
    this->name = copy.name;
    this->operatorsList = copy.operatorsList;
}

int Country::getMcc() const
{
    return mcc;
}

void Country::setMcc(int value)
{
    mcc = value;
}

int Country::getMnc_length() const
{
    return mnc_length;
}

void Country::setMnc_length(int value)
{
    mnc_length = value;
}

QString Country::getCode() const
{
    return code;
}

void Country::setCode(const QString &value)
{
    code = value;
}

QString Country::getName() const
{
    return name;
}

void Country::setName(const QString &value)
{
    name = value;
}

QList<Operator> Country::getOperatorsList() const
{
    return operatorsList;
}

void Country::setOperatorsList(const QList<Operator> &value)
{
    operatorsList = value;
}
