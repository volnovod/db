#include "treeitem.h"

TreeItem::TreeItem(const TreeItem::Data &data, TreeItem *parent)
    : itemData(data),
      parentItem(parent)
{
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

TreeItem *TreeItem::child(int number)
{
    if (number < 0 || number >= childItems.size())
        return nullptr;
    return childItems.at(number);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));
    return 0;
}

int TreeItem::columnCount() const
{
    return 1;
}

QVariant TreeItem::data() const
{
    switch (itemData.type) {
    case ID_CODE::COUNTRY:
        return itemData.countryName;
        break;
    case ID_CODE::OPERATOR:
        return itemData.operatorName + " (" + QString::number(itemData.mcc) + ", " + QString::number(itemData.mnc) + ")";
        break;
    default:
        return QVariant();
        break;
    }
}

bool TreeItem::insertChildren(int position, int count, int columns)
{
    Q_UNUSED(columns)
    if (position < 0 || position > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        Data data;
        TreeItem *item = new TreeItem(data, this);
        childItems.insert(position, item);
    }

    return true;
}

TreeItem *TreeItem::parent()
{
    return parentItem;
}

bool TreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

bool TreeItem::setData(const TreeItem::Data &value)
{
    itemData = value;
    return true;
}

TreeItem::Data TreeItem::getItemData() const
{
    return itemData;
}

void TreeItem::setOperatorName(QVariant name)
{
    itemData.operatorName = name.toString();
}
