#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "databaseaccessor.h"
#include "treemodel.h"
#include "customtreedelegate.h"
#include "editorwidget.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_dataBaseAccessor = new DataBaseAccessor();

    qRegisterMetaType<QList<Country>>("QList<Country>");

    connect(m_dataBaseAccessor, &DataBaseAccessor::needDataUpdate, this, &MainWindow::needDataUpdate);

    m_thread = new QThread();

    connect(m_thread, &QThread::started, m_dataBaseAccessor, &DataBaseAccessor::createConnection);
    connect(m_thread, &QThread::finished, m_thread, &QThread::deleteLater);

    connect(m_dataBaseAccessor, &DataBaseAccessor::setOperatorsModel, this, &MainWindow::setOperatosModel);

    m_dataBaseAccessor->moveToThread(m_thread);

    m_thread->start();


    ui->treeView->setItemDelegate(new CustomTreeDelegate());
    ui->treeView->setEditTriggers(QAbstractItemView::DoubleClicked);

}

MainWindow::~MainWindow()
{
    delete ui;
    disconnect(m_dataBaseAccessor, &DataBaseAccessor::needDataUpdate, this, &MainWindow::needDataUpdate);
    disconnect(m_dataBaseAccessor, &DataBaseAccessor::setOperatorsModel, this, &MainWindow::setOperatosModel);
    delete m_dataBaseAccessor;
    m_thread->quit();
    m_thread->wait();
    delete m_thread;
}

void MainWindow::setOperatosModel(QList<Country> data)
{
    m_model = new TreeModel(data);
    ui->treeView->setModel(m_model);
    //    ui->treeView->setModel(data);
    //    data->select();
}

void MainWindow::needDataUpdate(QString name, QString mcc, QString mnc)
{
    m_model->addOperatorToModel(name, mcc, mnc);
}

void MainWindow::setModel()
{

    m_model = new TreeModel(m_dataBaseAccessor->getData());
    ui->treeView->setModel(m_model);

}

void MainWindow::on_addbutton_clicked()
{
    EditorWidget* editor = new EditorWidget(m_model);
    editor->show();
    connect(editor, &EditorWidget::createOperator, m_dataBaseAccessor, &DataBaseAccessor::createOperator);
}
