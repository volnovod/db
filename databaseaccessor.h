#pragma once

#include <QObject>
#include <QtSql>

#include "operator.h"
#include "country.h"

class DataBaseAccessor : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseAccessor(QObject *parent = nullptr);
    ~DataBaseAccessor();




    void prepareData();


    QList<Country> getData() const;

signals:

    void setOperatorsModel(QList<Country> data);

    void needDataUpdate(const QString& name, const QString& mcc, const QString& mnc);

public slots:

    bool createConnection();
    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>());
    void createOperator(const QString& name, const QString& mcc, const QString& mnc);

private:

    QList<Operator> getOperatorsByMcc(int mcc);
    QList<Country> getCountries();
    QList<Country> data;

    QSqlDatabase dataBase;
};
