#pragma once
#include <QList>

#include "operator.h"


class Country
{

public:
    Country(int mcc = 0, int mnc_length = 0, QString code = "", QString name = "", QList<Operator> operatorsList = QList<Operator>());

    Country(const Country& copy);

    int getMcc() const;
    void setMcc(int value);

    int getMnc_length() const;
    void setMnc_length(int value);

    QString getCode() const;
    void setCode(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    //Data getData(id code);

    QList<Operator> getOperatorsList() const;
    void setOperatorsList(const QList<Operator> &value);

private:
    int mcc = 0;
    int mnc_length = 0;
    QString code = "";
    QString name = "";
    QList<Operator> operatorsList;
};
Q_DECLARE_METATYPE(Country)
