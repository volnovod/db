#pragma once

#include <QObject>

class Operator
{

public:
    Operator(int mcc = 0, int mnc = 0, QString name = "");

    int getMcc() const;
    void setMcc(int value);

    int getMnc() const;
    void setMnc(int value);

    QString getName() const;
    void setName(const QString &value);

private:

    int mcc = 0;
    int mnc = 0;
    QString name = "";

};
Q_DECLARE_METATYPE(Operator)
