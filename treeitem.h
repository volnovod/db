#pragma once

#include <QVariant>
#include <QVector>
#include "country.h"

class TreeItem
{
public:

        enum ID_CODE{
            COUNTRY = 0,
            OPERATOR,
        };

        struct Data
        {
            ID_CODE type = COUNTRY;
            int mcc = 0;
            int mncLength = 0;
            QString countryCode = QString();
            QString countryName = QString();

            QString operatorName = QString();
            int mnc = 0;
        };


    explicit TreeItem(const TreeItem::Data &data, TreeItem *parent = nullptr);
    ~TreeItem();

    TreeItem *child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data() const;
    bool insertChildren(int position, int count, int columns);
    TreeItem *parent();
    bool removeChildren(int position, int count);
    int childNumber() const;
    bool setData(const Data &value);


    TreeItem::Data getItemData() const;
    void setOperatorName(QVariant name);

private:
    QVector<TreeItem*> childItems;
    TreeItem::Data itemData;
    TreeItem *parentItem;
};
